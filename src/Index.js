import chalk from "chalk";
import * as mm from "music-metadata";
import util, { promisify } from "util";
const { resolve } = require("path");
const fs = require("fs");
const readdir = promisify(fs.readdir);
const stat = promisify(fs.stat);

async function getFiles(dir) {
  const subdirs = await readdir(dir);
  const files = await Promise.all(
    subdirs.map(async (subdir) => {
      const res = resolve(dir, subdir);
      return (await stat(res)).isDirectory() ? getFiles(res) : res;
    })
  );
  return files.reduce((a, f) => a.concat(f), []);
}

const log = console.log;

export const StartIndexing = () => {
  log("-- indexing registre --");
  const indexing_path = "/mnt/e/Users/Valentin/Music/Soundcloud";
  let json_data = [];
  // Using fs.exists() method to
  // check that the directory exists or not
  fs.exists(indexing_path, (exists) => {
    if (exists) {
      log(chalk.green("Directory exists.."));
    } else {
      fs.mkdirSync(indexing_path, { recursive: true });
    }
  });
  getFiles(indexing_path)
    .then(async (files) => {
      let filesFiltered = files.filter(
        (item) => item.includes(".mp3") || item.includes(".flac")
      );
      for (const file in filesFiltered) {
        const path_of_the_song = filesFiltered[file];
        let i = path_of_the_song.length;
        let arr = [];
        while (path_of_the_song[i] !== "/") {
          arr.push(path_of_the_song[i]);
          i--;
        }
        let str = arr.reverse().join("");
        log(str);
        // move into indexed
        await mm
          .parseFile(filesFiltered[file])
          .then((metadata) => {
            // console.log(util.inspect(metadata, { showHidden: false, depth: null }));
            const data = {
              artist_m: metadata.common.artist,
              title_m: metadata.common.title,
              title: str,
              duration: metadata.format.duration,
              bitrate: metadata.format.bitrate,
              oldPath: path_of_the_song,
              path: `${indexing_path}/${str}`,
            };
            log(data);
            json_data.push(data);
          })
          .catch((err) => {
            console.error(err.message);
          });
      }
      log(json_data);
      let music = JSON.stringify(json_data, null, 2);
      fs.writeFileSync("music_indexed.json", music);
    })
    .catch((e) => console.error(e));
};

export const SplitByBitrate = () => {
  let rawdata = fs.readFileSync("music_indexed.json");
  let musics = JSON.parse(rawdata);
  let inf320 = [];
  let equal320 = [];
  let sup320 = [];
  let trash = [];
  musics.map((music) => {
    if (music.bitrate < 320000) {
      inf320.push(music);
      return;
    } else if (music.bitrate == 320000) {
      equal320.push(music);
      return;
    } else if (music.bitrate > 320000) {
      sup320.push(music);
      return;
    } else {
      trash.push(music);
    }
  });
  fs.exists(`${__dirname}/data`, (exists) => {
    if (exists) {
      log(chalk.green("Directory exists.."));
    } else {
      fs.mkdirSync(`${__dirname}/data`, { recursive: true });
    }
  });

  let data = JSON.stringify(inf320, null, 2);
  fs.writeFileSync("./data/inf320.json", data);

  data = JSON.stringify(equal320, null, 2);
  fs.writeFileSync("./data/equal320.json", data);

  data = JSON.stringify(sup320, null, 2);
  fs.writeFileSync("./data/sup320.json", data);

    data = JSON.stringify(trash, null, 2);
    fs.writeFileSync('./data/trash.json', data);

    log("inf320: ", inf320.length)
    log("equal320: ", equal320.length)
    log("sup320: ", sup320.length)
    log("trash: ", trash.length)
}

export const printData = () => {
    let rawdata = fs.readFileSync('./data/inf320.json');
    let musics = JSON.parse(rawdata);
    log(chalk.blueBright("inf320: "), musics.length)

    rawdata = fs.readFileSync('./data/equal320.json');
    musics = JSON.parse(rawdata);
    log(chalk.blueBright("equal320: "), musics.length)


    rawdata = fs.readFileSync('./data/sup320.json');
    musics = JSON.parse(rawdata);
    log(chalk.blueBright("sup320: "), musics.length)


    rawdata = fs.readFileSync('./data/trash.json');
    musics = JSON.parse(rawdata);
    log(chalk.blueBright("trash: "), musics.length)
}
