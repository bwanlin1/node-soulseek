import chalk from 'chalk'
import Login from './Login'
import slsk from 'slsk-client'
import inquirer from 'inquirer'
import Soundcloud from 'soundcloud-api-client'

const log = console.log

const client_id = process.env.SOUNDCLOUD_ID

class SoundcloudUrl {
    constructor(url) {
        this.title, this.search, this.user = ""
        this.duration = 0
        this.tracks = {}
        this.url = url
        this.sc = new Soundcloud({ client_id })
    }

    async getTrack() {
        await this.sc.get('/resolve', { url: this.url }).then(tracks => {
            this.tracks = tracks
            this.duration = tracks.duration
            this.user = tracks.user.username
            let splitTitle = tracks.title.split('-')
            if (splitTitle.length == 1) {
                this.title = tracks.user.username.toLowerCase() + " - " + splitTitle[0].trim().toLowerCase()
            } else if (splitTitle.length > 1) {
                this.title = splitTitle[0].trim().toLowerCase() + " - " + splitTitle[1].trim().toLowerCase()
            } else {
                throw new Error("Can't retrieve title from url soundcloud.. sorry...")
            }
            this.search = this.title.split(' - ').join(' ')
        })
    }

    printTrack() {
        log(this.tracks)
    }

    printOpts() {
        log(this.title, " : ", this.duration, " : ", this.user, " : ", this.search)
    }
}

let Credentials = new Login()

const checkingUrlSC = (url) => {
    if (url.includes("https://soundcloud.com")) {
        if (url.includes('sets')) {
            log(chalk.yellow("it's a playlist.. not support..exiting"))
            process.exit()
        } else {
            log(chalk.blue("it's a song, gg"))
            return 0
        }
    } else {
        log(chalk.red("url is not a soundcloud's url.. exiting.."))
        process.exit()
    }
}

const searchAndDownload = (client, SC) => {
    return new Promise((resolve, reject) => {
        client.search({ req: SC.search, timeout: 4000 }, (err, res) => {
            if (err) return log(err)
            let alwaysMoreBitrate = res.filter(song => song.file.includes('.mp3') && song.slots == true)
            log(alwaysMoreBitrate)
            if (alwaysMoreBitrate.length > 0) {
                log(chalk.cyanBright("Downloading the track !(be patient i don't have the status)"))
                // download the track
                client.download({
                        file: alwaysMoreBitrate[0],
                        path: __dirname + '/' + SC.title
                    }, (err, data) => {
                        if (err) return log(err)
                        log(chalk.blueBright("Download complete for ", SC.title))
                        resolve()
                    })
            } else {
                log(chalk.red("no match found for ", SC.search))
                reject()
            }
        })
    })
}

const startModeSoundcloud = () => {
    Credentials.askCredentials().then(() => {
        log(chalk.green('Connecting to soulseek'));
        slsk.connect({ user: Credentials.login, pass: Credentials.pass }, async (err, client) => {
            if (err) {
                log(chalk.red('connection failed to soulseek...'))
                process.exit()
            }
            log(chalk.blue("Connected to soulseek"))
            while(42) {
                // prompt for url
                const urlQuestion = {
                    type: 'input',
                    name: 'url',
                    message: 'Soundcloud url',
                }
                const url = await inquirer.prompt(urlQuestion).then((urlAnswer) => {
                    return urlAnswer.url
                })
                if (checkingUrlSC(url) == 0) {
                    // connect to soundcloud api and get info about the url
                    const SC = new SoundcloudUrl(url)
                    await SC.getTrack().then(async () => {
                        SC.printOpts()
                        // connect to soulseek search and download
                        log(chalk.green("searching for ", SC.search))
                        await searchAndDownload(client, SC)
                    })
                }
            }
        }) 
    })
}

export default startModeSoundcloud