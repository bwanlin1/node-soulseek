import inquirer from 'inquirer'

const log = console.log

class Login {
    constructor() {
        let login, pass = ""
    }

    /**
     * Ask for credentials
     */
    async askCredentials() {
        const loginQuestion = {
            type: 'input',
            name: 'login',
            message: 'Login',
        };
        const pwdQuestion = {
            type: 'password',
            name: 'pwd',
            message: 'Password',
            mask: '*'
        };
        await inquirer.prompt(loginQuestion).then(async (loginAnswer) => {
            await inquirer.prompt(pwdQuestion).then((pwdAnswer) => {
                this.login = loginAnswer.login
                this.pass = pwdAnswer.pwd
            });
        });
    }
    getCredentials() {
        return { login: this.login, pass: this.pass }
    }
    printCredentials() {
        log(this.login, " -- ", this.pass)
    }
}

export default Login