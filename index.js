import { program } from 'commander'
import StartSoundcloud from './src/Soundcloud'
import { StartIndexing, SplitByBitrate, printData } from './src/Index'

const log = console.log

program.version('0.0.1', '-v, --version', 'output the current version')

program
    .command('files')
    .description('start the mode files of the program')
    .action(() => {
        log("command called files")
    })

program
    .command('index')
    .description('index the path given and create a json with all data music collected')
    .action(() => {
        StartIndexing()
    })

program
    .command('split')
    .description('split music by bitrate after indexing')
    .action(() => {
        SplitByBitrate()
    })

program
    .command('print')
    .description('print data of the collection')
    .action(() => {
        printData()
    })

program
    .command('soundcloud')
    .description('start the mode soundcloud of the program')
    .action(() => {
        StartSoundcloud()
    })

program.parse(process.argv)